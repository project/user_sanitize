## CONTENTS OF THIS FILE

  - Introduction
  - Requirements
  - Installation
  - Configuration
  - Maintainers

 ## INTRODUCTION

 The User Sanitize module adds customizable user sanitization functions.

* For a full description of the module, visit the project page:
  <https://www.drupal.org/project/user_sanitize>

* To submit bug reports and feature suggestions, or to track changes:
  <https://www.drupal.org/project/issues/user_sanitize>


 ## REQUIREMENTS

 This module requires no modules outside of Drupal core.


 ## INSTALLATION

 * Install the User Sanitize module as you would normally install a contributed
   Drupal module. Visit:
   https://www.drupal.org/docs/extending-drupal/installing-modules
   for further information.

 ## CONFIGURATION

  1. Navigate to Administration > Extend and enable the module.
  2. Navigate to Administration > Configuration > Development > User Sanitize
     to manage settings.

Options:
  - Ability to exclude users by role
  - Ability to exclude users by uid
  - Granular functions at the field level:
  - Include/exclude fields to sanitize
  - Custom sanitizers (Empty, Defined, Name, Random Word, Random Sentence)
  - Granular functions at the field level
  - Enforce lower case
  - Add specific suffixes to all values
  - Run from UI/Drush


 ## MAINTAINERS

  - Matt Gill (mattgill) - https://www.drupal.org/u/mattgill
  - Andre Bonon(andre.bonon) - https://www.drupal.org/u/andrebonon
  - Gabriel Passarelli (gabrielpassarelli) - https://www.drupal.org/u/gabrielpassarelli

Supporting organizations:

  - Care Quality Commission - https://www.drupal.org/care-quality-commission
  - PA Consulting - https://www.drupal.org/pa-consulting
  - ImageX Media - https://www.drupal.org/imagex
