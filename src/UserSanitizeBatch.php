<?php

namespace Drupal\user_sanitize;

use Drupal\Component\Utility\Random;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\user\Entity\User;

/**
 * Batch command for user_sanitize.
 */
class UserSanitizeBatch {

  /**
   * Build batch function.
   */
  public static function build(bool $from_drush = FALSE): void {
    $config = \Drupal::config('user_sanitize.settings');

    // Get our settings from the form.
    foreach ($config->getRawData() as $settingId => $setting) {
      $applied_settings[$settingId] = $setting;
    }

    // Array of roles NOT to sanitize.
    $excluded_roles = empty($applied_settings['settings']['exclusion']['excluded_roles']) ? [] : $applied_settings['settings']['exclusion']['excluded_roles'];
    $excluded_roles = array_keys(array_filter($excluded_roles));
    // Array of uids NOT to sanitize.
    $excluded_uids = empty($applied_settings['settings']['exclusion']['excluded_ids']) ? [] : str_getcsv($applied_settings['settings']['exclusion']['excluded_ids']);
    $current_user_id = \Drupal::currentUser()->id();
    if (!$from_drush && $current_user_id != 0) {
      $excluded_uids[] = $current_user_id;
    }
    // Always skip user 1
    $excluded_uids[] = 1;

    $batch_builder = (new BatchBuilder())
      ->setTitle(t('Sanitizing users...'))
      ->setFinishCallback([static::class, 'finish'])
      ->setInitMessage(t('Loading users to sanitize.'))
      ->setProgressMessage(t('Processed @current out of @total.'))
      ->setErrorMessage(t('Batch has encountered an error'));

    $batch_builder->addOperation(
      [static::class, 'process'],
      [$excluded_roles, $excluded_uids],
    );

    batch_set($batch_builder->toArray());

    // Called from Drush.
    if ($from_drush) {
      // Start processing the batch operations.
      drush_backend_batch_process();
    }
  }

  /**
   * Process batch callback function.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function process($excluded_roles, $excluded_uids, &$context): void {
    if (!isset($context['sandbox']['progress'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current_node'] = 1;
      $context['sandbox']['max'] = \Drupal::entityQuery('user')->count()
        ->execute();
    }

    $start = $context['sandbox']['progress'];
    $limit = 50;
    $query = \Drupal::entityQuery('user');
    $query->range($start, $limit);
    $uids = $query->execute();
    $uids_without_excluded = array_diff($uids, $excluded_uids);
    $users = User::loadMultiple($uids_without_excluded);

    $config = \Drupal::config('user_sanitize.settings');
    $fields = $config->get('fields');

    foreach ($users as $user) {
      $user_roles = $user->getRoles();
      $user_has_excluded_roles = !empty(array_intersect($user_roles, $excluded_roles));
      // Skip users with excluded roles.
      if (!$user_has_excluded_roles) {
        self::sanitizeUser($user, $fields);
      }

      // Store some result for post-processing in the finished callback.
      $context['results'][] = $user->id();

      // Update our progress information.
      $context['sandbox']['progress']++;
      $context['sandbox']['current_node'] = $user->id();
      $context['message'] = t('Sanitizing users @current/@total', [
        '@current' => $context['sandbox']['progress'],
        '@total' => $context['sandbox']['max'],
      ]);
    }

    // Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Batch finish callback function.
   */
  public static function finish($success, $results, $operations): void {
    if ($success) {
      $message = t('@count users successfully sanitized:', ['@count' => count($results)]);
      \Drupal::messenger()->addStatus($message);
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $message = t('An error occurred while processing %error_operation with arguments: @arguments some users might have escaped !', [
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE),
      ]);
      \Drupal::messenger()->addError($message);
    }
  }

  /**
   * Sanitize a specific user based on user_sanitize settings.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private static function sanitizeUser(User $user, $fields): void {
    // For each field for the user:
    foreach ($fields as $field => $fieldSetting) {
      // Skip any field we don't want to sanitize.
      if ($fieldSetting['enabled'] != TRUE) {
        continue;
      }
      // If we get to this point, we want to sanitize this field.
      // Get the value to set this field to.
      $newValue = self::getSanitizedValue($fieldSetting['params'], $user);

      // Drupal specific fields (name, pass, email) need to be set with
      // specific functions, other fields can be set using the set function.
      switch ($field) {
        case 'name':
          $user->setUsername($newValue);
          break;

        case 'pass':
          $user->setPassword($newValue);
          break;

        case 'mail':
          $user->setEmail($newValue);
          break;

        default:
          $user->set($field, $newValue);
          break;
      }
    }

    $user->save();
  }

  /**
   * Method to get a sanitized string value.
   */
  private static function getSanitizedValue($params, $user): string {
    $randomiser = new Random();
    $value = '';

    // Depending on the time of sanitizer used, get the value from Drupal's
    // random class.
    switch ($params['sanitizer']) {
      case 'name':
        $value = $randomiser->name();
        break;

      case 'word':
        $value = $randomiser->word((int) $params['word_count']);
        break;

      case 'sentence':
        $value = $randomiser->word((int) $params['sentence_count']);
        break;
    }

    // Convert to lowercase if required.
    if ($params['lowercase'] == TRUE) {
      $value = strtolower($value);
    }

    // Add the suffix if specified.
    if ($params['suffix'] == TRUE) {
      $value .= \Drupal::token()
        ->replace($params['suffix_text'], ['user' => $user]);
    }

    return $value;
  }

}
